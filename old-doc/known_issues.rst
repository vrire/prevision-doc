############
Known issues
############

This page lists known issues with Prevision.io, along with ways you can avoid or recover from these issues.
Those issues are given a high priority to the Prevision.io development team and will be solved in an upcoming release.

- There can be small differences between the unit and batch predict for the same data and model.

- The decision tree simple model will not accurately display the modalities for categorical features, instead
  only showing the encoded modalities. (red, green, blue will show up as 1, 2, 3)

- Downloading large files (> 1GB) from the notebooks can sometimes fail silently
  (the downloaded file might be incomplete).

- For multi-classification experiments, the bivariate analysis shows the top 3 classes *per bin* instead of the top
  3 classes overall.

- The .svg format plot download will sometimes fail.

- Dataset upload will fail with empty datasets (only header). In that case the "Parsed" status will never complete or fail.

- Some advanced metrics (F2, F3, F4 Score, Lift, ...) don't have a star system related to them

Experimental features
---------------------

- ALN timeseries experiments might crash when given an ID column
