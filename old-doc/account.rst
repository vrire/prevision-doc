##########
My account
##########

You can access your account details by clicking on the following icon:

.. image:: _static/images/task_bar_account.png
   :align: center

In this screen, you will find the information you filled in when you registered.
This information can be modified at any time, except for your email address.

.. image:: _static/images/account.png
   :align: center

You can also manage your password, activate two-factor authentication, or consult the login connections to your account.

In any case, if you encounter any problems, do not hesitate to contact us at the following address: support@prevision.io