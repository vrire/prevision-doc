******
Images
******

In the prevision.io platform, you can train several experiments type using images :

regression
classification
multi-classification
image detection

For the first three kinds of training, the user flow is similar to the corresponding tabular experiments. You will just need in addition to add an image folder corresponding to the train dataset in the new experiment screen.

.. figure:: img/slected_img.png
   :alt: image alt text

Image experiments configuration
===============================

The image experiments training workflow is similar to the tabular corresponding experiments (except for image detection). Please refer to the tabular datatype training read the doc section in order to get information about the train settings.
However this similarity, some differences are notable. On the field configuration options, an image path is required.This image path is the link between the tabular train dataset and the corresponding images.

.. figure:: img/slected_fi_img.png
   :alt: image alt text


Image detection experiments
===========================

In the prevision.io platform, a particular kind of image experiment allows you to train models that are able to recognize and boxing on an image a particular object.

In order to train image detection experiments you will need to have an image folder and a tabular document including :

* the image path
* the object label
* the bounding box coordinates

.. figure:: img/imod.png
   :alt: image alt text

The image detection experiment training configuration is simpler than for other training. In advance options, only training performances choice between quick, normal & advanced is available.
