Notebooks
=========

Introduction
^^^^^^^^^^^^


Prevision.io offers various tools to enable data science use cases to be carried out. Among these tools, there are notebooks and production tools.
Notebooks are not scoped into projects. You can access notebooks by clicking on the notebook button on the left main menu. Then you will be redirected to the following page.

.. figure:: img/loads.png
   :alt: Load notebook


Jupyter (python)
^^^^^^^^^^^^^^^^

For Python users, a JUPYTERLAB environment (https://github.com/jupyterlab/jupyterlab) is available in Prevision.io

Note that a set of packages is preinstalled on your instance (list: https://previsionio.readthedocs.io/fr/latest/_static/ressources/packages_python.txt), particularly the previsionio package that encapsulates the functions using the tool’s native APIs. Package documentation link: https://prevision-python.readthedocs.io/en/latest/

Jupyter (R studio)
^^^^^^^^^^^^^^^^^^

For R users, a R STUDIO environment (https://www.rstudio.com) is available in Prevision.io

Note that a set of packages is preinstalled on your instance (list: https://previsionio.readthedocs.io/fr/latest/_static/ressources/packages_R.txt), particularly the previsionio package that encapsulates the functions that use the tool’s native APIs. Package documentation link: https://previsionio.readthedocs.io/fr/latest/_static/ressources/previsionio.pdf
