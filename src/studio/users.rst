User
====


.. figure:: img/user.png
   :alt: image alt text

* Language : switch the language between french or english
* Profile : navigate to you profil information such as email and password
* administration & API key : available only for admins
* documentation : ReadTheDoc redirection
* Terms and conditions
* log out 
